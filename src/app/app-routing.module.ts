import {  NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { LandingComponent } from './components/landing/landing.component';
import { InsurancePlansComponent } from './components/insurancePlans/insurancePlans.component';
import { content } from "./shared/routes/content-routes";

const routes: Routes = [
  
  {
    path:"",
    redirectTo:"/pega",
    pathMatch:"full"
  },
  {
    path:"",
    component: AppComponent,
    children: content
  },
  {
    path:"home",
    component: HomeComponent,
  },
  {
    path:"pega",
    component: LandingComponent,
  },
  {
    path:"plans",
    component: InsurancePlansComponent,
  },
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

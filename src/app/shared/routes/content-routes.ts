import { Routes } from "@angular/router";
export const content: Routes = [
    {
        path:"CarInsuranceModule",
        loadChildren: () =>
        import("../../components/carInsurance/carInsurance.module").then((m) => m.CarInsuranceModule)
    }
]
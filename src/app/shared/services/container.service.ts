import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import {HttpClient} from '@angular/common/http'


@Injectable({
    providedIn: 'root'
})

export class ContainerService{
RankedResults:any;

constructor(private http:HttpClient){
}
//Get Offer
getOffer(ContainerName:any,customerId:any,Variant:any,ZipCode:any,regonumber:any,pyNote:any){
    console.log(Variant);
    const body = { 
        "ContainerName":ContainerName,
        "SubjectID":customerId,
        "ContextName":"Customer",
        "Channel":"Web",
        "Direction":"Inbound",
        "Category":"car",
        "Variant":Variant,
        "postalCode":ZipCode,
        "regonumber": regonumber,
        "pyNote": pyNote
        
    };
    return this.http.post<any>('https://cap-fsdemo.pegatsdemo.com/prweb/api/PegaMKTContainer/V3/Container', body);
}
getParticularOfferDetails(customerId:any,offerName:any){
    console.log(customerId);
    const body = { 
        "ContainerName":"MotorInsurancePlansContainer",
        "SubjectID":customerId,
        "ContextName":"Customer",
        "Channel":"Web",
        "Direction":"Inbound",
        "pyNote":offerName,
        
        
    };
    return this.http.post<any>('https://cap-fsdemo.pegatsdemo.com/prweb/api/PegaMKTContainer/V3/Container', body);
}
getcaptureResponse(cutomerId:any,offerDetails:any,outcome:any){
    console.log(offerDetails);
    console.log(cutomerId);
    this.RankedResults = offerDetails['ContainerList'][0]['RankedResults'][0];
    console.log(this.RankedResults);
    const body = {
    "RankedResults":[{"ContainerName":"MotorInsurancePlansContainer","Outcome":outcome,
    "ProtectiveRidingClothsFee": this.RankedResults['ProtectiveRidingClothsFee'],
    "Issue": this.RankedResults['Issue'],
    "InteractionID": this.RankedResults['InteractionID'],
    "EmergencyTransportAndAccomodationCover": this.RankedResults['EmergencyTransportAndAccomodationCover'],
    "CustomerCost": this.RankedResults['CustomerCost'],
    "ContentFormat": this.RankedResults['ContentFormat'],
    "NewMotorcycleCoverDuration": this.RankedResults['NewMotorcycleCoverDuration'],
    "LostOrStolenKeysFee": this.RankedResults['LostOrStolenKeysFee'],
    "Label": this.RankedResults['Label'],
    "Premium": this.RankedResults['Premium'],
    "Benefits": this.RankedResults['Benefits'],
    "WhyRelevant": this.RankedResults['WhyRelevant'],
    "NewCarReplacement": this.RankedResults['NewCarReplacement'],
    "UninsuredMotoristExtension": this.RankedResults['UninsuredMotoristExtension'],
    "Name": this.RankedResults['Name'],
    "ShortDescription": this.RankedResults['ShortDescription'],
    "GroupID": this.RankedResults['GroupID'],
    "PaidAudienceName": this.RankedResults['PaidAudienceName'],
    "Excess": this.RankedResults['Excess'],
    "Identifier": this.RankedResults['Identifier'],
    "Placement": this.RankedResults['Placement'],
    "AccidentalDeathBenefit": this.RankedResults['AccidentalDeathBenefit'],
    "JourneyStep": this.RankedResults['JourneyStep'],
    "EmergencyTransportAndAccomodationCoverFee": this.RankedResults['EmergencyTransportAndAccomodationCoverFee'],
    "Propensity": this.RankedResults['Propensity'],
    "AgentCompensation": this.RankedResults['AgentCompensation'],
    "ClassIdentifier": this.RankedResults['ClassIdentifier'],
    "Priority": this.RankedResults['Priority'],
    "Channel": this.RankedResults['Channel'],
    "RoadSideAssistance": this.RankedResults['RoadSideAssistance'],
    "BundleName": this.RankedResults['BundleName'],
    "ImageURL": this.RankedResults['ImageURL'],
    "NewCarReplacementIndicator": this.RankedResults['NewCarReplacementIndicator'],
    "LifetimeGuaranteeOnRepairs": this.RankedResults['LifetimeGuaranteeOnRepairs'],
    "ChildSeatOrBabyCapsuleFee": this.RankedResults['ChildSeatOrBabyCapsuleFee'],
    "Variant": this.RankedResults['Variant'],
    "ChildSeatOrBabyCapsule": this.RankedResults['ChildSeatOrBabyCapsule'],
    "InternalCost": this.RankedResults['InternalCost'],
    "Journey": this.RankedResults['Journey'],
    "Group": this.RankedResults['Group'],
    "ExcessBasic": this.RankedResults['ExcessBasic'],
    "SafeDriverRewards": this.RankedResults['SafeDriverRewards'],
    "Category": this.RankedResults['Category'],
    "SubjectName": this.RankedResults['SubjectName'],
    "UninsuredMotoristExtensionFee": this.RankedResults['UninsuredMotoristExtensionFee'],
    "AccidentalCleanup": this.RankedResults['AccidentalCleanup'],
    "Direction": this.RankedResults['Direction'],
    "SlideCarCoverFee": this.RankedResults['SlideCarCoverFee'],
    "FreedomToNominateRepairer": this.RankedResults['FreedomToNominateRepairer'],
    "Pricing": this.RankedResults['Pricing'],
    "JourneyStage": this.RankedResults['JourneyStage'],
    "NewMotorcycleCover": this.RankedResults['NewMotorcycleCover'],
    "LostOrStolenKeys": this.RankedResults['LostOrStolenKeys'],
    "ExcessFee": this.RankedResults['ExcessFee'],
    "NotAtFaultHire": this.RankedResults['NotAtFaultHire'],
    "PersonalEffectsCoverFee": this.RankedResults['PersonalEffectsCoverFee'],
    "EligibilityDescription": this.RankedResults['EligibilityDescription'],
    "PersonalEffectsCover": this.RankedResults['PersonalEffectsCover'],
    "AccidentalCleanupFee": this.RankedResults['AccidentalCleanupFee'],
    "Rank": this.RankedResults['Rank'],
    "BundleParent": this.RankedResults['BundleParent'],
    "ExcessWindshield": this.RankedResults['ExcessWindshield'],
    "ExcessDescription": this.RankedResults['ExcessDescription'],
    "ProtectiveRidingCloth": this.RankedResults['ProtectiveRidingCloth'],
    "Treatment": this.RankedResults['Treatment'],
    "CampaignID": this.RankedResults['CampaignID'],
    "SlideCarCover": this.RankedResults['SlideCarCover'],
    "OfferValue": this.RankedResults['OfferValue'],
    "ClickThroughURL": this.RankedResults['ClickThroughURL'],
    "SubjectID": cutomerId,
    "DecisionTime": this.RankedResults['DecisionTime'],
    "ContextName": this.RankedResults['ContextName'],
    "PostCode":this.RankedResults['PostCode']
}]}

    return this.http.post<any>('https://cap-fsdemo.pegatsdemo.com/prweb/api/PegaMKTContainer/V3/CaptureResponse', body);
}
}


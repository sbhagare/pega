import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import {HttpClient} from '@angular/common/http'


@Injectable({
    providedIn: 'root'
})

export class CaptureCustomerDetailsService{

  
constructor(private http:HttpClient){
}
postCustomerDetails(body:any){
    return this.http.post('https://cap-fsdemo.pegatsdemo.com/prweb/api/CDHSvcPackage/V1/capturecustomerprospectinfo', body);
}
getCustomerDetails(customerId:any){
    console.log(customerId);
    return this.http.get<any>('https://cap-fsdemo.pegatsdemo.com/prweb/api/CDHSvcPackage/V1/capturecustomerprospectinfo?CustomerID='+customerId );
}

putCustomerDetails(body:any){
    return this.http.put('https://cap-fsdemo.pegatsdemo.com/prweb/api/CDHSvcPackage/V1/capturecustomerprospectinfo', body, { responseType: 'text' });
}
}


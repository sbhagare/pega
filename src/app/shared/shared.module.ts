import { NgModule } from '@angular/core';
import {HeaderComponent} from './components/header/header.component';
import {FooterComponent} from './components/footer/footer.component';
import {TopbarComponent} from './components/topbar/topbar.component';
import { RouterModule, Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
  declarations: [
    TopbarComponent,HeaderComponent,FooterComponent
  ],
  imports: [RouterModule,BrowserModule
  ],
  exports:[HeaderComponent,FooterComponent,TopbarComponent],
  bootstrap: []
})
export class SharedModule { }

import { Component } from '@angular/core';
import { Router, NavigationEnd ,ActivatedRoute,ActivationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Pega';
  currentRoute: any;
  idOffername:any;
  constructor(private route: ActivatedRoute,private router: Router){
    
    this.router.events.subscribe((e) => {
      if(e instanceof ActivationStart ){
        this.idOffername=e.snapshot.params.id
        console.log(e.snapshot.params.id);
      }
      if (e instanceof NavigationEnd) {
        this.currentRoute = e.url;
      }
    });
  }
}

import {  NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CarInsuranceComponent } from './carInsurance.component';
import { ThirdpartyComponent } from './thirdparty/thirdparty.component';
import { ComprehensiveComponent } from './comprehensive/comprehensive.component';


const routes: Routes = [
{
   path:'',
   children:[
    {
      path:'carInsurance',
      component:CarInsuranceComponent
    },
    {
      path:'thirdParty',
      component:ThirdpartyComponent
    },
    {
      path:'comprehensive',
      component:ComprehensiveComponent
   }
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarInsuranceRoutingModule { }

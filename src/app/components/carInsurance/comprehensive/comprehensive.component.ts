import { Component, OnInit } from '@angular/core';
import { ContainerService } from '../../../shared/services/container.service';
import { ActivatedRoute ,Router} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
// import { CustomerDetailsComponent } from '../../customerDetails/customerDetails.component';

@Component({
  selector: 'app-comprehensive',
  templateUrl: './comprehensive.component.html',
  styleUrls: ['./comprehensive.component.scss']
})
export class ComprehensiveComponent implements OnInit {
  rankedResults:any; 
  public responseArray:any={};
  pyNote:any;
  subjectId:any;
  data:any;
  arrayLength:any;
  containerName:any;
  
  constructor(private route:ActivatedRoute,private router:Router, public ContainerService :ContainerService,private cookies:CookieService) {
    //this.cookies.deleteAll();

    var getcustomerId=this.cookies.get('setcustomerId');
    console.log(getcustomerId);
    if(getcustomerId == '' || getcustomerId == null || getcustomerId == 'undefined' ){
      this.subjectId=''
    }
    else{
      var getcustomerId=this.cookies.get('setcustomerId');
     // var customeridArray = JSON.parse(getcustomerId);
      //var customerId = customeridArray['CustomerID'];
      console.log('getcustomerId='+getcustomerId);
        if(getcustomerId != '' || getcustomerId != null || getcustomerId != 'undefined'){
          this.subjectId = getcustomerId;  
          console.log(JSON.stringify(this.subjectId));
        }  
    }

    var Variant = 'Comprehensive';
    this.route.queryParams.subscribe(params => {
      this.pyNote = params['pyNote'];
      this.containerName='MotorInsurancePlansContainer';
      this.ContainerService.getOffer(this.containerName,this.subjectId,Variant,'','',this.pyNote).subscribe(offer => {
        console.log(offer);
        this.rankedResults=offer['ContainerList'][0]['RankedResults'];
        this.arrayLength=this.rankedResults.length
        console.log(this.rankedResults.length);
     
          let Name = new Array();
          let Label = new Array();
          let Premium = new Array();
          let Excess = new Array();
          let NotAtFaultHire = new Array();
          let LifetimeGuaranteeOnRepairs = new Array();
          let PersonalEffectscover = new Array();
          let NewCarReplacement	= new Array();
          let LostOrStolenKeysFee = new Array();
          let AccidentalDeathBenefit = new Array();
          let ChildSeatOrBabyCapsuleFee = new Array();
          let EmergencyTransportAndAccomodationCoverFee = new Array();
          let RoadSideAssistance = new Array();
          let FreedomToNominateRepairer	= new Array();
          let SafeDriverRewards	= new Array();
          let AccidentalCleanup	= new Array();
          let UninsuredMotoristExtension= new Array();
          
        
      
        for (let i = 0; i < this.rankedResults.length; i++) {
          console.log(this.rankedResults[i]['Label']);
          Name.push(this.rankedResults[i]['Name']);
          Label.push(this.rankedResults[i]['Label']);
          Premium.push(this.rankedResults[i]['Premium']);
          Excess.push(this.rankedResults[i]['ExcessFee']);
          NotAtFaultHire.push(this.rankedResults[i]['NotAtFaultHire']);
          LifetimeGuaranteeOnRepairs.push(this.rankedResults[i]['LifetimeGuaranteeOnRepairs']);
          PersonalEffectscover.push(this.rankedResults[i]['PersonalEffectsCoverFee']);
          NewCarReplacement.push(this.rankedResults[i]['NewCarReplacement']);
          LostOrStolenKeysFee.push(this.rankedResults[i]['LostOrStolenKeysFee']);
          AccidentalDeathBenefit.push(this.rankedResults[i]['AccidentalDeathBenefit']);
          EmergencyTransportAndAccomodationCoverFee.push(this.rankedResults[i]['EmergencyTransportAndAccomodationCoverFee']);
          ChildSeatOrBabyCapsuleFee.push(this.rankedResults[i]['ChildSeatOrBabyCapsuleFee']);
          RoadSideAssistance.push(this.rankedResults[i]['RoadSideAssistance']);
          FreedomToNominateRepairer.push(this.rankedResults[i]['FreedomToNominateRepairer']);
          SafeDriverRewards.push(this.rankedResults[i]['SafeDriverRewards']);
          AccidentalCleanup.push(this.rankedResults[i]['AccidentalCleanup']);
          UninsuredMotoristExtension.push(this.rankedResults[i]['UninsuredMotoristExtension']);

          
        }
          this.responseArray['Name']=Name;
          this.responseArray['Label']=Label;
          this.responseArray['Premium']=Premium;
          this.responseArray['Excess']=Excess;
          this.responseArray['NotAtFaultHire']=NotAtFaultHire;
          this.responseArray['LifetimeGuaranteeOnRepairs']=LifetimeGuaranteeOnRepairs;
          console.log(this.responseArray['LifetimeGuaranteeOnRepairs']);
          this.responseArray['PersonalEffectsCoverFee']=PersonalEffectscover;
          this.responseArray['NewCarReplacement']=NewCarReplacement;
          this.responseArray['LostOrStolenKeysFee']=LostOrStolenKeysFee
          this.responseArray['AccidentalDeathBenefit']=AccidentalDeathBenefit;
          this.responseArray['EmergencyTransportAndAccomodationCoverFee']=EmergencyTransportAndAccomodationCoverFee;
          this.responseArray['ChildSeatOrBabyCapsuleFee']=ChildSeatOrBabyCapsuleFee;
          this.responseArray['RoadSideAssistance']=RoadSideAssistance;
          this.responseArray['FreedomToNominateRepairer']=FreedomToNominateRepairer;
          this.responseArray['SafeDriverRewards']=SafeDriverRewards;
          this.responseArray['AccidentalCleanup']=AccidentalCleanup;
          this.responseArray['UninsuredMotoristExtension']=UninsuredMotoristExtension;
      });
    });
  }
  getQuote(offername:any){
    console.log(offername);
    this.subjectId=this.cookies.get('setcustomerId');
    console.log(this.subjectId);
    this.router.navigate(['/customerDetail/'+offername]);
    // if(this.subjectId == '' || this.subjectId == 'null' || this.subjectId == undefined){
    //   this.router.navigate(['/customerDetail/'+offername]);
    // }
    // else{
    //   this.ContainerService.getParticularOfferDetails(this.subjectId,offername).subscribe(offer => {
    //     console.log(offer);     
    //     var offerDetails = offer;
    //     var outcome = 'Pending';
    //     this.ContainerService.getcaptureResponse(this.subjectId,offerDetails,outcome).subscribe(offerReponce => {
    //       console.log(offerReponce);
    //       this.router.navigate(['/summary']);
    //     });   
    //   }); 
    // }
  }
  ngOnInit(): void {
  }

}

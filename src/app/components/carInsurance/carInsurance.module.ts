import { NgModule } from '@angular/core';
import {ThirdpartyComponent} from '../carInsurance/thirdparty/thirdparty.component';
import {ComprehensiveComponent} from '../carInsurance/comprehensive/comprehensive.component';
import { RouterModule, Routes } from '@angular/router';
import { CarInsuranceRoutingModule } from './carInsurnace-routing.module';
import { CarInsuranceComponent } from '../carInsurance/carInsurance.component';
import { SharedModule } from '../../shared/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';  
import { FormsModule } from '@angular/forms'; 

@NgModule({
  declarations: [
    ComprehensiveComponent,ThirdpartyComponent,CarInsuranceComponent
  ],
  imports: [RouterModule,SharedModule,NgbModule,CommonModule,CarInsuranceRoutingModule,FormsModule],
  exports:[ComprehensiveComponent,ThirdpartyComponent,CarInsuranceComponent],
  bootstrap: []
})
export class CarInsuranceModule { }

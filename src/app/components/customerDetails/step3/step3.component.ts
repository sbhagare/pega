import { Component, OnInit } from '@angular/core';
import {CustomerDetailsComponent} from '../../customerDetails/customerDetails.component';
import { DataService } from '../data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss']
})
export class Step3Component implements OnInit {

  data: any;
  subscription: Subscription | undefined;
  constructor(private customerDetailsComponent:CustomerDetailsComponent,private dataService: DataService) {
    
  }
  ngOnInit(): void {
    this.subscription = this.dataService.currentdata.subscribe(res => this.data = res);
  }
  onSubmit(x:any){
    //console.log(x);
    this.data['RegularRider'] = x.regularRider;
    this.data['License'] = x.license;
    this.data['YearsOfLicense'] = x.yearsOfLicense;
    this.data['OwnDriver'] = x.ownDriver;
    this.data['RestrictAge'] = x.restrictAge;
   
    this.dataService.changeData(this.data);
    this.customerDetailsComponent.next();
  }
  back(){
    this.customerDetailsComponent.back();
  }
}

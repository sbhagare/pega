import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import {CustomerDetailsComponent} from '../../customerDetails/customerDetails.component';
import { DataService } from '../data.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss']
})
export class Step1Component implements OnInit {
  currentStep=1;
  create=false;
  data: any={};
  subscription: Subscription | undefined;
  RegoNUmber:any;
  data123:any={};
  url:any;
  isDisplayed:boolean=true;
  isDisabled:boolean=true;

  constructor(private route:ActivatedRoute,private customerDetailsComponent:CustomerDetailsComponent,private dataService: DataService) {
    this.route.params.subscribe(params => {
      this.url = params['id'];
      console.log(params['id']) 
      if(this.url == 'getQuote'){
        this.isDisplayed=false;
        this.isDisabled=false;
      }
      else{
        this.isDisplayed=true;
        this.isDisabled=true;
      }
    });
  }
  ngOnInit(): void {
    this.subscription = this.dataService.currentdata.subscribe(res => this.data = res);
  }

  onSubmit(x:any){
    console.log(x);
    this.data['RegoNUmber'] = x.regoNumber;
    this.data['TypeConver'] = x.typeConver;
    this.data['policyStartDate'] = x.dp;
    this.data['VehicleCoverage'] = x.vehicleCoverage;
    this.data['NumberOfClaims'] = x.numberOfClaims;
    this.dataService.changeData(this.data);
    this.customerDetailsComponent.next();
  } 
  
}


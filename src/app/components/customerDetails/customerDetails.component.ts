import { Component, OnInit } from '@angular/core';
import { CaptureCustomerDetailsService } from '../../shared/services/capturecustomerDetails.service';
import { ContainerService } from '../../shared/services/container.service';
import { CookieService } from 'ngx-cookie-service';
import { DataService } from './data.service';
import { BehaviorSubject } from 'rxjs';
import { Router,ActivatedRoute } from '@angular/router';


@Component({
  selector: 'app-customerDetails',
  templateUrl: './customerDetails.component.html',
  styleUrls: ['./customerDetails.component.scss']
})
export class CustomerDetailsComponent implements OnInit {
 
  currentStep=1;
  create=false;
  private dataSource = new BehaviorSubject({});
  currentdata = this.dataSource.asObservable();
  customerDetails:any;
  data:any;
  getData:any;
  offerName:any;
  cutomerId:any
  currentUrl:any;
  customeridArray:any;

  constructor(private router: Router,private route: ActivatedRoute,private captureCustomerDetails :CaptureCustomerDetailsService,private containerService: ContainerService,private cookies: CookieService,private dataService:DataService) {
    //this.cookies.deleteAll('setcustomerId');
    this.currentUrl=this.router.url;
    this.route.params.subscribe(params => {
      console.log(params['id']) 
      this.offerName = params['id'];
    });
    
    console.log(this.currentUrl);
    var getcustomerId=this.cookies.get('setcustomerId');
    console.log(getcustomerId);
    
    if(getcustomerId == '' || getcustomerId == null || getcustomerId == 'undefined' ){
      getcustomerId=''
    }
    else{
      var getcustomerId=this.cookies.get('setcustomerId');
      console.log(getcustomerId);
     // var customerId = this.customeridArray;
      console.log('getcustomerId='+getcustomerId);
        if(getcustomerId != '' || getcustomerId != null || getcustomerId != 'undefined'){
          this.getData = this.getCustmerDetail(getcustomerId);  
          console.log(JSON.stringify(this.getData));
        }  
    }
    
  }

  ngOnInit(): void {
  }
   //POST API
   postCustomerDetail(data: any){
    console.log(data);
    console.log(data['Gender']);
    const body = {
      "ContextName":"Customer",
      "Channel":"Web",
      "BirthDate":data['DateOfBirth']['year']+''+data['DateOfBirth']['month']+''+data['DateOfBirth']['day'],
      "HasChildren":"true",
      "EmploymentStatus":data['EmploymentStatus'],
      "AnnualIncome":"10000",
      "IsGraduate":"True",
      "MaritalStatus":"Married",
      "NoOfDependents":"2",
      "NumberOfChildren":"1",
      "Profession":"Civil",
      "RelationWithDependent":"Father",
      "ResidenceType":"2BHK",
      "CreditScore":"500",
      "FirstName":data['FirstName'],
      "LastName":data['LastName'],
      "Gender":data['Gender'],
      "IsAlone":data['LiveAlone'],
      "ContactDetails": 
      {
      "City":"",
      "PrimaryAddressLine1":"Jubilee Hills",
      "PrimaryAddressLine2": "Beside Metro Station",
      "PrimaryCity": data['LiveAddress'],
      "PrimaryCountryCode":"IND",
      "PrimaryCountry":"India",
      "PrimaryEmail":data['EmailContact'],
      "PrimaryMobilePhone":data['PhoneNumber'],
      "PrimaryState":"AP",         
      "Post_Code":"533004" 
      },
      "CustomerHabits":
      {
      "IsAlcoholic":data['Alcohol'],
      "IsSmoker":data['doSmoke']
      },
      "CustomerVehicleData":
      {
      "AverageKMTravelledPerYear":data['AvgNumber'],
      "CanRestrictRiderAge":data['RestrictAge'],
      "ClassOfLicense":data['License'],
      "HasClaimsinPast":"true",
      "PreviousClaimCount":data['NumberOfClaims'],
      "HasMarketChangesInVehicle":data['AftermarketModifications'],
      "HasUnClaimedDamagesOrRepairs":data['UnrepairedAccident'],
      "InsuranceCoverType":data['TypeConver'],
      "IsCommercialVehicle":data['BusinessActivities'],
      "IsInsuredinPastYear":data['VehicleCoverage'],
      "IsOwnVehicle":"true",
      "IsRegularRider":data['RegularRider'],
      "IsSecurityDevicePresent":data['SecurityDevices'],
      "IsVehicleUnderFinance":data['Finance'],
      "LicenseValidtyInYears":data['YearsOfLicense'],
      "OptedVehicalInsurance":"true",
      "OwnsMultipleVehicles":data['OwnDriver'],
      "PolicyStartDate":data['policyStartDate']['year']+''+data['policyStartDate']['month']+''+data['policyStartDate']['day'],
      "VehicleRegistrationNumber": data['RegoNUmber'],
      "VehicleType":"CAR",
      "VehParkingLocDuringNight": data['VehicleParked'],
      "VehValueAfterUpgrade":data['Accessories']     
      }
  }   
    this.captureCustomerDetails.postCustomerDetails(body).subscribe(customerDetails => {
      this.customerDetails=customerDetails;
      console.log(this.customerDetails['CustomerID']);
      this.cutomerId = this.customerDetails['CustomerID'];
      
      this.cookies.set('setcustomerId',this.cutomerId,365);      

      this.getPerticularOfferDetails(this.cutomerId,this.offerName,data);
      
    });  
  }
  //Get perticular Offer
  getPerticularOfferDetails(cutomerId:any,offerName:any,data:any){
    console.log(data['TypeConver']);
    this.containerService.getParticularOfferDetails(cutomerId,offerName).subscribe(offer => {
      console.log(offer);
      if(offer['ContainerList'][0]['Message'] != "No offers available"){
        this.CaptureResponse(cutomerId,offer);
      }
      else{
        var url =data['TypeConver'];
        this.router.navigate(['/'+url]);
      }
    });  
  }

  //CaptureResponse
  CaptureResponse(cutomerId:any,offerDetails:any){
    // if(){
      
    // }
    var outcome = 'Pending';
    this.containerService.getcaptureResponse(cutomerId,offerDetails,outcome).subscribe(offerReponce => {
      console.log(offerReponce);
      this.router.navigate(['/summary/'+this.offerName]);
      //this.CaptureResponse(offer);
    });
  }

  //GET API
  getCustmerDetail(getcustomerId:any){
    console.log(getcustomerId);
    this.captureCustomerDetails.getCustomerDetails(getcustomerId).subscribe(customerDetails => {
      console.log(customerDetails);
      this.data = customerDetails;
      this.data['RegoNUmber'] = this.data['CustomerVehicleData']['VehicleRegistrationNumber'];
      this.data['TypeConver'] =  this.data['CustomerVehicleData']['InsuranceCoverType'];
      var dateOfPolicy= this.data['CustomerVehicleData']['PolicyStartDate'];
      dateOfPolicy = JSON.stringify(dateOfPolicy);
      this.data['policyStartDate']= {
        "year": parseInt(dateOfPolicy.substring(1, 5)),
        "month": parseInt(dateOfPolicy.substring(5, 7)),
        "day": parseInt(dateOfPolicy.substring(7, 9))
      };
      this.data['VehicleCoverage'] =  this.data['CustomerVehicleData']['HasClaimsinPast'];
      console.log();
      this.data['NumberOfClaims'] =  this.data['CustomerVehicleData']['PreviousClaimCount'];
      
      this.data['SecurityDevices'] = this.data['CustomerVehicleData']['IsSecurityDevicePresent'];
      this.data['AftermarketModifications'] = this.data['CustomerVehicleData']['HasMarketChangesInVehicle'];
      this.data['Accessories'] = this.data['CustomerVehicleData']['VehValueAfterUpgrade'];
      this.data['Finance'] = this.data['CustomerVehicleData']['IsVehicleUnderFinance'];
      this.data['VehicleParked'] = this.data['CustomerVehicleData']['VehParkingLocDuringNight'];
      this.data['UnrepairedAccident'] = this.data['CustomerVehicleData']['HasUnClaimedDamagesOrRepairs'];
      this.data['BusinessActivities'] = this.data['CustomerVehicleData']['IsCommercialVehicle'];
      this.data['AvgNumber'] = this.data['CustomerVehicleData']['AverageKMTravelledPerYear'];
      
      this.data['RegularRider'] = this.data['CustomerVehicleData']['IsRegularRider'];
      this.data['License'] = this.data['CustomerVehicleData']['ClassOfLicense'];
      this.data['YearsOfLicense'] = this.data['CustomerVehicleData']['LicenseValidtyInYears'];
      this.data['OwnDriver'] = this.data['CustomerVehicleData']['OwnsMultipleVehicles'];
      this.data['RestrictAge'] = this.data['CustomerVehicleData']['CanRestrictRiderAge'];
      
      this.data['FirstName'] = this.data['FirstName'];
      this.data['LastName'] = this.data['LastName'];
      //this.data['DateOfBirth'] = this.data['BirthDate'];
      var dateOfBirth= this.data['BirthDate'];
      dateOfBirth = JSON.stringify(dateOfBirth);
      this.data['DateOfBirth']= {
        "year": parseInt(dateOfBirth.substring(1, 5)),
        "month": parseInt(dateOfBirth.substring(5, 7)),
        "day": parseInt(dateOfBirth.substring(7, 9))
      };
      this.data['EmailContact'] = this.data['ContactDetails']['PrimaryEmail'];
      this.data['PhoneNumber'] = this.data['ContactDetails']['PrimaryMobilePhone'];
      this.data['Gender'] = this.data['Gender'];
      console.log(this.data['Gender']);
      this.data['EmploymentStatus'] = this.data['EmploymentStatus'];
      this.data['LiveAddress'] = this.data['ContactDetails']['PrimaryCity'];

      this.data['doSmoke'] = this.data['CustomerHabits']['IsSmoker'];
      this.data['Alcohol'] = this.data['CustomerHabits']['IsAlcoholic'];
      this.data['LiveAlone'] = this.data['IsAlone'];
      this.data['IncomeRange'] = '10000';

      this.dataService.changeData(this.data);  
      console.log(this.data);
    });
  }
//Put API
putCustomerDetail(data: any,customerId:any){
  console.log('I am in Put method'+data);
  console.log('I am in Put method'+JSON.stringify(data));
  console.log(customerId);

  const body = {
    "ContextName":"Customer",
    "Channel":"Web",
    "CustomerID": customerId,
    "BirthDate":data['DateOfBirth']['year']+''+data['DateOfBirth']['month']+''+data['DateOfBirth']['day'],
    "HasChildren":"true",
    "EmploymentStatus":data['EmploymentStatus'],
    "AnnualIncome":"10000",
    "IsGraduate":"True",
    "MaritalStatus":"Married",
    "NoOfDependents":"2",
    "NumberOfChildren":"1",
    "Profession":"Civil",
    "RelationWithDependent":"Father",
    "ResidenceType":"2BHK",
    "CreditScore":"500",
    "FirstName":data['FirstName'],
    "LastName":data['LastName'],
    "Gender":data['Gender'],
    "IsAlone":data['LiveAlone'],
    "ContactDetails": 
    {
    "City":"",
    "PrimaryAddressLine1":"Jubilee Hills",
    "PrimaryAddressLine2": "Beside Metro Station",
    "PrimaryCity": data['LiveAddress'],
    "PrimaryCountryCode":"IND",
    "PrimaryCountry":"India",
    "PrimaryEmail":data['EmailContact'],
    "PrimaryMobilePhone":data['PhoneNumber'],
    "PrimaryState":"AP",         
    "Post_Code":"533004" 
    },
    "CustomerHabits":
    {
    "IsAlcoholic":data['Alcohol'],
    "IsSmoker":data['doSmoke']
    },
    "CustomerVehicleData":
    {
    "AverageKMTravelledPerYear":data['AvgNumber'],
    "CanRestrictRiderAge":data['RestrictAge'],
    "ClassOfLicense":data['License'],
    "HasClaimsinPast":"true",
    "PreviousClaimCount":data['NumberOfClaims'],
    "HasMarketChangesInVehicle":data['AftermarketModifications'],
    "HasUnClaimedDamagesOrRepairs":data['UnrepairedAccident'],
    "InsuranceCoverType":data['TypeConver'],
    "IsCommercialVehicle":data['BusinessActivities'],
    "IsInsuredinPastYear":data['VehicleCoverage'],
    "IsOwnVehicle":"true",
    "IsRegularRider":data['RegularRider'],
    "IsSecurityDevicePresent":data['SecurityDevices'],
    "IsVehicleUnderFinance":data['Finance'],
    "LicenseValidtyInYears":data['YearsOfLicense'],
    "OptedVehicalInsurance":"true",
    "OwnsMultipleVehicles":data['OwnDriver'],
    "PolicyStartDate":data['policyStartDate']['year']+''+data['policyStartDate']['month']+''+data['policyStartDate']['day'],
    "VehicleRegistrationNumber": data['RegoNUmber'],
    "VehicleType":"CAR",
    "VehParkingLocDuringNight": data['VehicleParked'],
    "VehValueAfterUpgrade":data['Accessories']     
    }
}   
  this.captureCustomerDetails.putCustomerDetails(body).subscribe(customerDetails => {
    console.log(JSON.stringify(customerDetails));
    this.customerDetails=customerDetails;
    console.log(this.customerDetails);

    this.cookies.set('setcustomerId',this.customerDetails,365);
    var updatedCustomerId = this.customerDetails;

    this.getPerticularOfferDetails(updatedCustomerId,this.offerName,data);

    //this.router.navigate(['/summary']);
    console.log(this.customerDetails);
  });
  }
  back(){
     this.create = false;
    if(this.currentStep == 2){
      this.currentStep = 1;
    }
    if(this.currentStep == 3){
      this.currentStep = 2;
    }
    if(this.currentStep == 4){
      this.currentStep = 3;
    }
    if(this.currentStep == 5){
      this.currentStep = 4;
    }
  }
  next(){
    if(this.currentStep == 1){
      this.currentStep =2;
    }
    else if(this.currentStep == 2){
      console.log('step-2222');
      this.currentStep =3;
    }
    else if(this.currentStep == 3){
      this.currentStep =4;
    }
    else if(this.currentStep == 4){
      this.currentStep =5;
    }
    else if(this.currentStep == 5){
      this.currentStep =6;
    }
   
  }

}

import { Component, OnInit } from '@angular/core';
import {CustomerDetailsComponent} from '../../customerDetails/customerDetails.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { DataService } from '../data.service';
import { Subscription } from 'rxjs';
import { CookieService } from 'ngx-cookie-service';
import { ActivatedRoute, Router } from '@angular/router';
import { ViewChild, TemplateRef} from '@angular/core';

@Component({
  selector: 'app-step4',
  templateUrl: './step4.component.html',
  styleUrls: ['./step4.component.scss']
})

export class Step4Component implements OnInit {

  finishbtn:any;
  isDisplayed:boolean=true;
  data: any;
  subscription: Subscription | undefined;  
  url:any;
  subjectId:any;

  @ViewChild("mymodal") private contentRef: TemplateRef<Object> | undefined;

  constructor(private router:Router,private route:ActivatedRoute,private customerDetailsComponent:CustomerDetailsComponent,private cookies: CookieService,private modalService: NgbModal,private dataService: DataService) { 
   //this.cookies.deleteAll();
    this.route.params.subscribe(params => {
      this.url = params['id'];
      if(this.url == 'getQuote'){
        this.isDisplayed=false;
      }
      else{
        this.isDisplayed=true;
      }
    });
  }

  ngOnInit(): void {
    this.subscription = this.dataService.currentdata.subscribe(res => this.data = res);
  }
  
 
  onSubmit(x:any){
    console.log(x);
    this.data['FirstName'] = x.firstName;
    this.data['LastName'] = x.lastName;
    this.data['DateOfBirth'] = x.dateOfBirth;
    this.data['EmailContact'] = x.emailContact;
    this.data['PhoneNumber'] = x.phoneNumber;
    this.data['Gender'] = x.gender;
    this.data['EmploymentStatus'] = x.employmentStatus;
    this.data['LiveAddress'] = x.liveAddress;
    
    if(this.isDisplayed == false){
      this.open(this.contentRef);
    }
    else{
      var getcustomerId=this.cookies.get('setcustomerId');
      console.log(getcustomerId);
      if(getcustomerId == '' || getcustomerId == null || getcustomerId == 'undefined' ){
        console.log(getcustomerId);
        this.customerDetailsComponent.postCustomerDetail(this.data);
        this.subjectId=''
        
      }
      else{
        var getcustomerId=this.cookies.get('setcustomerId');
        console.log(getcustomerId);
          if(getcustomerId != '' || getcustomerId != null || getcustomerId != 'undefined'){
            this.subjectId =getcustomerId;
            this.customerDetailsComponent.putCustomerDetail(this.data, this.subjectId);
          }  
      }
    }

  this.dataService.changeData(this.data);
  this.finishbtn = document.getElementById('ContinueBtn');

  //Get Quote Button
    if(this.finishbtn.value =='Finish'){

        var getcustomerId=this.cookies.get('setcustomerId');
        console.log(getcustomerId);
        if(getcustomerId == '' || getcustomerId == null || getcustomerId == 'undefined' ){
          console.log(getcustomerId);
          this.customerDetailsComponent.postCustomerDetail(this.data);
          this.subjectId=''
          
        }
        else{
          var getcustomerId=this.cookies.get('setcustomerId');
          var customeridArray = JSON.parse(getcustomerId);
          console.log(customeridArray);
          var customerId = customeridArray['CustomerID'];
          console.log('getcustomerId='+customerId);
            if(customerId != '' || customerId != null || customerId != 'undefined'){
              this.customerDetailsComponent.putCustomerDetail(this.data, customerId);
            }  
        }
      
        
    }
  }
  open(content:any) {
    console.log(content);
    this.finishbtn = document.getElementById('ContinueBtn');
    if(this.finishbtn.value !='Finish'){
      this.modalService.open(content, { size: 'lg', centered: true });
    }
  }

  
  onSubmitModal(x:any){
    //console.log(x);
    this.data['doSmoke'] = x.doSmoke;
    this.data['Alcohol'] = x.Alcohol;
    this.data['LiveAlone'] = x.LiveAlone;
    this.data['IncomeRange'] = x.IncomeRange;
    
    //Get Quote Button
    var getcustomerId=this.cookies.get('setcustomerId');
        console.log(getcustomerId);
        if(getcustomerId == '' || getcustomerId == null || getcustomerId == 'undefined' ){
          console.log(getcustomerId);
          this.customerDetailsComponent.postCustomerDetail(this.data);
          this.subjectId=''
          
        }
        else{
          var getcustomerId=this.cookies.get('setcustomerId');
          console.log(getcustomerId);
          console.log('getcustomerId='+getcustomerId);
            if(getcustomerId != '' || getcustomerId != null || getcustomerId != 'undefined'){
              this.customerDetailsComponent.putCustomerDetail(this.data, getcustomerId);
            }  
        }
    
    this.modalService.dismissAll();   
  
  }
  skipFunction(){
    this.finishbtn = document.getElementById('ContinueBtn');
    this.finishbtn.value='Finish';
    this.modalService.dismissAll();
  }
  back(){
    this.customerDetailsComponent.back();
  }
}

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CaptureCustomerDetailsService } from '../../shared/services/capturecustomerDetails.service';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class DataService {

  private dataSource = new BehaviorSubject({});
  currentdata = this.dataSource.asObservable();
  customerDetails:any;
  data:any;
  getData:any;

  constructor(private captureCustomerDetails: CaptureCustomerDetailsService,private cookies: CookieService) { 
    
   
  }

  changeData(data: any) {
    this.dataSource.next(data)
  }
  
 
}
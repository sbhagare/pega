import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import {Step1Component} from '../customerDetails/step1/step1.component';
import {Step2Component} from '../customerDetails/step2/step2.component';
import {Step3Component} from '../customerDetails/step3/step3.component';
import {Step4Component} from '../customerDetails/step4/step4.component';
import {SummaryComponent} from '../customerDetails/summary/summary.component';
import {PaymentComponent} from '../customerDetails/payment/payment.component';
import { PlansComponent } from '../customerDetails/plans/plans.component';
import { CommonModule } from '@angular/common';  
import {CustomerDetailsComponent} from '../customerDetails/customerDetails.component';
import { SharedModule } from '../../shared/shared.module';
import { CarInsuranceRoutingModule } from './customerDetails-routing.module';

@NgModule({
  declarations: [
    Step1Component,CustomerDetailsComponent,Step2Component,Step3Component,Step4Component,SummaryComponent,PlansComponent,PaymentComponent
  ],
  imports: [
    BrowserModule,NgbModule,FormsModule,CommonModule,
    SharedModule,CarInsuranceRoutingModule
  ],
  exports:[Step1Component,Step2Component,Step3Component,Step4Component,PlansComponent,SummaryComponent],
  providers: [],
  bootstrap: []
})
export class CustomerDetailsModule { }

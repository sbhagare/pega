import { Component, OnInit } from '@angular/core';
import {CustomerDetailsComponent} from '../../customerDetails/customerDetails.component';
import { DataService } from '../data.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss']
})
export class Step2Component implements OnInit {
  create=false;
  data: any;
  subscription: Subscription | undefined;
  
  constructor(private customerDetailsComponent:CustomerDetailsComponent,private dataService: DataService) { 
  }
  ngOnInit(): void {
    this.subscription = this.dataService.currentdata.subscribe(res => this.data = res);
  }
  onSubmit(x:any){
    console.log(x);
    this.data['SecurityDevices'] = x.securityDevices;
    this.data['AftermarketModifications'] = x.aftermarketModifications;
    this.data['Accessories'] = x.accessories;
    this.data['Finance'] = x.finance;
    this.data['VehicleParked'] = x.vehicleParked;
    this.data['UnrepairedAccident'] = x.unrepairedAccident;
    this.data['BusinessActivities'] = x.businessActivities;
    this.data['AvgNumber'] = x.avgNumber;
    this.dataService.changeData(this.data);

    this.customerDetailsComponent.next();
  }
  back(){
    this.customerDetailsComponent.back();
  }
 
}

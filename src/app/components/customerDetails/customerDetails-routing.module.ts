import {  NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {CustomerDetailsComponent} from './customerDetails.component';
import {SummaryComponent} from './summary/summary.component';
import {PaymentComponent} from '../customerDetails/payment/payment.component';


const routes: Routes = [
{
   path:'',
   children:[
    {
      path:"customerDetail/:id",
      component: CustomerDetailsComponent,
    },
    {
      path:"summary/:id",
      component: SummaryComponent,
    },
    {
      path:"summary",
      component: SummaryComponent,
    },
    {
      path:"payment",
      component: PaymentComponent,
    },
  ]
}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CarInsuranceRoutingModule { }

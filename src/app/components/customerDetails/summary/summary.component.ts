import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ContainerService } from 'src/app/shared/services/container.service';


@Component({
  selector: 'app-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  offerName: any = "";
  customerId: any = "";
  offerData:any;
  containerName:any;
  subjectId:any;
  addonPlans:any;
  offerPrice:any;
  Variant:any;
  message:any;

  constructor(private router: Router, private route: ActivatedRoute,private cookies: CookieService,private containerService: ContainerService,) {
    this.route.params.subscribe(params => {
      this.offerName = params['id'];
    });
    this.containerName='AddonPlansContainer';
    this.subjectId='Cust-16';
    console.log('Subject id='+this.subjectId+'####');
    this.containerService.getOffer(this.containerName,this.subjectId,'','','','').subscribe(offer => {
      //console.log(offer['ContainerList'][0]['RankedResults']);
      this.addonPlans=offer['ContainerList'][0]['RankedResults'];
     console.log(this.addonPlans);
      
      // this.message=offer['ContainerList'][0]['Message'];
      // if(this.message != "No offers available"){
      //   this.addonPlans=offer['ContainerList'][0]['RankedResults'];
      //   this.Variant = offer['ContainerList'][0]['RankedResults'][0]['Variant'];
      //   this.offerName = offer['ContainerList'][0]['RankedResults'][0]['Name'];
      //   console.log(this.addonPlans);
      // }
      
      
    });
    this.customerId = this.cookies.get('setcustomerId');

  }

  ngOnInit(): void {
    this.getParticularOfferDetails(this.customerId,this.offerName);
  }

//Get perticular Offer
getParticularOfferDetails(cutomerId:any,offerName:any){
  this.containerService.getParticularOfferDetails(cutomerId,offerName).subscribe(offer => {
    console.log(offer);
    this.offerPrice = offer['ContainerList'][0]['RankedResults'][0]['Premium'];
    this.Variant = offer['ContainerList'][0]['RankedResults'][0]['Variant'];
    this.offerData=offer;
    // if(offer['ContainerList'][0]['Message'] != "No offers available"){
    //   this.UpdatePlanStatus(cutomerId,offer);
    // }
  });  
}

 //UpdatePlanStatus as Accepted
 UpdatePlanStatus(){
  var outcome = 'Accepted';
  this.containerService.getcaptureResponse(this.customerId,this.offerData,outcome).subscribe(offerReponce => {
    console.log(offerReponce);
    this.router.navigate(['/payment/']);
    //this.CaptureResponse(offer);
  });
}

}


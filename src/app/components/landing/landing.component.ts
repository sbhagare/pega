import { Component, OnInit } from '@angular/core';
import { ContainerService } from '../../shared/services/container.service';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  starRating = 0; 
  rankedResults:any;
  getData:any;
  pendingOffer:any;
  data:any;
  ZipCode:any;
  RegoNumber:any;
  pyNote:any;
  message:any;
  subjectId:any;
  Variant:any;
  offerName:any;
  containerName:any;

  constructor(private router: Router,public ContainerService :ContainerService,private cookies: CookieService) { 
    //this.cookies.deleteAll('setcustomerId');
    getcustomerId=this.cookies.get('setcustomerId');
    console.log(this.cookies.get('setcustomerId'));
    if(getcustomerId == '' || getcustomerId == null || getcustomerId == 'undefined' ){
      this.subjectId=''
    }
    else{
      var getcustomerId=this.cookies.get('setcustomerId');
      console.log('getcustomerId='+getcustomerId);
        if(getcustomerId != '' || getcustomerId != null || getcustomerId != 'undefined'){
          this.subjectId = getcustomerId;  
          console.log(JSON.stringify(this.subjectId));
        }  
    }

    this.pyNote='Pending';
    this.containerName='MotorInsurancePlansContainer';
    this.ContainerService.getOffer(this.containerName,this.subjectId,'','','',this.pyNote).subscribe(offer => {
      console.log(offer);
      this.message=offer['ContainerList'][0]['Message'];
      if(this.message != "No offers available"){
        this.rankedResults=offer['ContainerList'][0]['RankedResults'];
        this.Variant = offer['ContainerList'][0]['RankedResults'][0]['Variant'];
        this.offerName = offer['ContainerList'][0]['RankedResults'][0]['Name'];
        console.log(this.rankedResults);
      }
        
    });
  }
  onSubmit(x:any){
   console.log(x);
   //this.pendingOffer=x.Zipcode,x.rego;
   this.router.navigate(['/plans'],{ queryParams: {ZipCode: x.Zipcode, regoNumber: x.rego}});
   
  }
  ngOnInit(): void {
  }

}

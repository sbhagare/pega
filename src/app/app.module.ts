import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { CarInsuranceRoutingModule } from './components/carInsurance/carInsurnace-routing.module';
import { CustomerDetailsModule } from './components/customerDetails/customerDetails.module';
import { SharedModule } from './shared/shared.module';
import { AppComponent } from './app.component';
import { HttpClient,HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './components/home/home.component';
import { LandingComponent } from './components/landing/landing.component';
import { InsurancePlansComponent } from './components/insurancePlans/insurancePlans.component';
import { FormsModule } from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import {RouterModule} from '@angular/router';
import { CommonModule } from '@angular/common';  
//import { CarInsuranceModule } from './components/carInsurance/carInsurance.module';
import { DataService } from './components/customerDetails/data.service';


@NgModule({
  declarations: [
    AppComponent,HomeComponent,LandingComponent,InsurancePlansComponent
  ],
  imports: [
    BrowserModule,AppRoutingModule,NgbModule,HttpClientModule,FormsModule,CustomerDetailsModule,
    SharedModule,RouterModule,CommonModule,BrowserAnimationsModule,CarInsuranceRoutingModule
  ],
  providers: [CookieService,DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
